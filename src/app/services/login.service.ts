import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { NgForm } from "@angular/forms";
import { map, Observable } from "rxjs";
import { User } from "../models/user";
import { UserResponse } from "../models/userReponse";

@Injectable({
    providedIn:'root'
})
export default class LoginService{

    private _currentUser:User = {email:"sweetstate.com",firstName:"",lastName:"",profilePicUrl:"",userName:""}


    get currentUser():User{
        return this._currentUser
    }

    constructor(private readonly http:HttpClient){}

    loginAttempt(form:NgForm):void{

      // offload this to keycloak
      this._currentUser = {email:"dummyuser.com",firstName:form.value.userName,lastName:"later",profilePicUrl:"https://xsgames.co/randomusers/assets/avatars/male/60.jpg",userName:""}
    }
}