import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { Joke } from '../models/joke';
import { JokeResponse } from '../models/jokeResponse';

@Injectable({
  providedIn: 'root'
})
export class JokesService {

  private _jokes: Joke[] = []
  private JOKES_URL: string = 'https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&type=twopart'

  constructor(private readonly http: HttpClient) { }

  getJoke(): void {
    this.http.get<JokeResponse>(this.JOKES_URL)
      .pipe(
        map(response => {
          let parsedJoke: Joke = {
            setup: response.setup,
            delivery: response.delivery,
            id: response.id,
            rating: '🤡'
          }
          return parsedJoke
        }))
      .subscribe({
        next: response => { this.jokes.push(response) },
        error: error => console.log(error)
      })
  }

  get jokes(): Joke[] {
    return this._jokes
  }

  rateJoke(rating:string){
    this._jokes[this._jokes.length-1].rating = rating
  }


}
