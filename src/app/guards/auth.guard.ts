import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { flags } from "@oclif/command";
import { Observable } from "rxjs";
import keycloak from "src/keycloak";
import LoginService from "../services/login.service";


@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    

    constructor(private readonly router: Router, private readonly loginService:LoginService) {
    }

    canActivate(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {

        // yes you
        // this.loginService.currentUser.lastName==='later'
        if (keycloak.authenticated) {
            return true
        }
        // no you can't were you go so that you can
        else {
            this.router.navigateByUrl('/login')
            return false
        }
    }

}