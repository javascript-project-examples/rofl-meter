import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JokeReviewComponent } from './joke-review.component';

describe('JokeReviewComponent', () => {
  let component: JokeReviewComponent;
  let fixture: ComponentFixture<JokeReviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JokeReviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JokeReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
