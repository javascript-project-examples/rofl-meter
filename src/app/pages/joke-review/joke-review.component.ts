import { Component, OnInit } from '@angular/core';
import { KeycloakTokenParsed } from 'keycloak-js';
import { Joke } from 'src/app/models/joke';
import { User } from 'src/app/models/user';
import { JokesService } from 'src/app/services/jokes.service';
import keycloak, { KeycloakTokenParsedExtended } from 'src/keycloak';

@Component({
  selector: 'app-joke-review',
  templateUrl: './joke-review.component.html',
  styleUrls: ['./joke-review.component.css']
})
export class JokeReviewComponent implements OnInit {

  constructor(private readonly jokesService:JokesService) { }

  currentUser:User = {  userName:'string',
  email:'string',
  firstName:'string',
  lastName:'string',
  profilePicUrl:'string'}


  ngOnInit(): void {
    this.currentUser.firstName = keycloak.tokenParsed?.name 
    this.currentUser.email = keycloak.tokenParsed?.email 
  }

  get jokes(): Joke[] {
    return this.jokesService.jokes
  }

}
