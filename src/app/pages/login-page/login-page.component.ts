import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  constructor(private readonly http:HttpClient) { }

  ngOnInit(): void {
  }

  loginWithKeycloak(): void{

    // redirect to keycloak to login
    keycloak.login()
  }

  logoutWithKeycloak(): void{

    // redirect to keycloak to login
    keycloak.logout()
  }

  ShowToken(){
    console.log(keycloak.token)
    console.log(keycloak.tokenParsed)
  }

  UseToken(){

    const headers = new HttpHeaders({
      'Authorization': `Bearer ${keycloak.token}`
    })

    this.http.get('https://acc-products-fake.herokuapp.com/api/products',{headers:headers})
    .subscribe(
      {
        next:(reponse)=>{
          console.log(reponse)
        },
        error:(error)=>{console.log(error)}
      }
    )
  }
}
