import { NgModule, } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http'

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.router.module';
import { HomeContentComponent } from './components/home-content/home-content.component';
import { JokeListItemComponent } from './components/joke-list-item/joke-list-item.component';
import { JokeListComponent } from './components/joke-list/joke-list.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { JokePageComponent } from './pages/joke-page/joke-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { FormsModule } from '@angular/forms';
import { JokeReviewComponent } from './pages/joke-review/joke-review.component';
import { JokesReviewListComponent } from './components/jokes-review-list/jokes-review-list.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeContentComponent,
    JokeListItemComponent,
    JokeListComponent,
    LoginFormComponent,
    HomePageComponent,
    LoginPageComponent,
    JokePageComponent,
    JokeReviewComponent,
    JokesReviewListComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
