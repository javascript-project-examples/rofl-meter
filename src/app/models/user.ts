
export type User =  {
    userName:string|undefined,
    email:string|undefined,
    firstName:string|undefined,
    lastName:string|undefined,
    profilePicUrl:string|undefined
}