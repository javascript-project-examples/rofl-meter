export type Joke= {
    setup:string,
    delivery:string,
    id:number,
    rating: string
}