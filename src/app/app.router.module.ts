import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { HomePageComponent } from "./pages/home-page/home-page.component";
import { JokePageComponent } from "./pages/joke-page/joke-page.component";
import { JokeReviewComponent } from "./pages/joke-review/joke-review.component";
import { LoginPageComponent } from "./pages/login-page/login-page.component";


const routes: Routes = [
    { path: '', pathMatch:'full', redirectTo:"/home" },
    { path: 'home', component: HomePageComponent },
    { path: 'login', component: LoginPageComponent },
    { path: 'review', component: JokeReviewComponent, canActivate:[AuthGuard] },
    { path: 'jokes', component: JokePageComponent, canActivate:[AuthGuard] }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }