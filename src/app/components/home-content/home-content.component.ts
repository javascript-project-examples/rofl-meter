import { Component } from "@angular/core";
import LoginService from "src/app/services/login.service";

@Component({
    selector:'app-home-content',
    templateUrl: './home-content.component.html',
    styleUrls: ['./home-content.component.css']
})
export class HomeContentComponent{
    constructor(private readonly login:LoginService){}

    loggedIn:boolean = false
    message:string = 'Hello NG'
    picUrl:string='https://preview.redd.it/19fq7c002w021.png?auto=webp&s=3e0a27751f560581f575619ba337a0d6a124096a'

    dailyTodos:string [] = ['Exercise', 'Read', 'Meditate','Walk']

    


}