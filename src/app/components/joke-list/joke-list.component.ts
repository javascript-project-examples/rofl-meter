import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Joke } from 'src/app/models/joke';
import { User } from 'src/app/models/user';
import { JokesService } from 'src/app/services/jokes.service';
import LoginService from 'src/app/services/login.service';

@Component({
  selector: 'app-joke-list',
  templateUrl: './joke-list.component.html',
  styleUrls: ['./joke-list.component.css']
})
export class JokeListComponent implements OnInit {

  jokeIndexer: number = 0

  // joke will get passed down to the child
  currentJoke: Joke = {
    setup: "some joke",
    delivery: 'haha',
    id: 6,
    rating:'😐'
  }

  constructor(private readonly http: HttpClient,
    private readonly loginService: LoginService,
    private readonly jokesService: JokesService) { }


  ngOnInit(): void {
    // load data from the joke api
    this.jokesService.getJoke()
    this.currentJoke = this.jokes[0]
  }

  handleNextJoke(): void {
    // fetch the next joke
    this.jokesService.getJoke()
    this.jokeIndexer++
  }

  get currentUser(): User {
    return this.loginService.currentUser
  }

  get jokes(): Joke[] {
    return this.jokesService.jokes
  }

  handleJokeUpvote(): void {
    this.jokesService.rateJoke('🤣')
  }

  handleJokeDownvote(): void {
    this.jokesService.rateJoke('😐')
  }



}
