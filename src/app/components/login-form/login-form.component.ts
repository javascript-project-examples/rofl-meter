import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { map, tap } from 'rxjs';
import { User } from 'src/app/models/user';
import { UserResponse } from 'src/app/models/userReponse';
import LoginService from 'src/app/services/login.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  constructor(private readonly http: HttpClient, private readonly loginService: LoginService, private readonly router: Router) { }

  ngOnInit(): void {
  }

  get currentUser(): User {
    return this.loginService.currentUser
  }

  login(form: NgForm): void {
    this.loginService.loginAttempt(form)
    this.router.navigate(['jokes'])
  }
}
