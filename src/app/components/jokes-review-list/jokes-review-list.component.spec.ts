import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JokesReviewListComponent } from './jokes-review-list.component';

describe('JokesReviewListComponent', () => {
  let component: JokesReviewListComponent;
  let fixture: ComponentFixture<JokesReviewListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JokesReviewListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JokesReviewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
